The module will show the button file explorer in the link field widget.
you can configure the path file in the widget settings,
in the display mode file browser can show folder.
Use the jQuery File Browser for the file browser.

### How to use:

- Add link field.
- Select file browser in widget.
- Select file you want to add.
- Right click for add folder.
- Add block with share folder name in public://
