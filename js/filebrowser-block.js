(function (Drupal, $, once) {
  'use strict';

  Drupal.behaviors.explorerblock = {
    attach: function (context, settings) {
      $(once('browser_block', '.browser-block', context)).each(function () {
        let url_scan = $(this).data('url_scan');
        let url = $(this).data('url');
        let all = $(this).data('all');
        var rootFolder = $(this).data('folder').replace('\\', '/');
        let google_viewer = $(this).data('google_viewer');
        let urlGoogle = 'https://docs.google.com/gview?embedded=true&url=';
        let googledocExtension = ['doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx'];
        let openExtension = ['txt', 'pdf', 'docx', 'xlsx', 'pptx', 'odt', 'ods', 'odp', 'csv',
          'jpg', 'jpeg', 'png', 'gif', 'bmp', 'svg',
          'mp3', 'wav', 'aac', 'ogg',
          'mp4', 'webm', 'ogv', 'mov',
        ];
        rootFolder = decodeURI(rootFolder);
        let that = $(this);
        $.post(url_scan, {directory: rootFolder, all: all}, function (env) {
          var current = {};
          function get(path) {
            current = env;
            browser.walk(path, function (file) {
              if (current[file] != undefined) {
                current = current[file];
              }
            });
            return current;
          }

          that.browse({
            root: '/',
            separator: '/',
            contextmenu: true,
            name: 'filestystem',
            refresh_timer: 0,
            dir: async function (path) {
              let dir = get(path);
              var result = {files: [], dirs: []};
              if ($.isPlainObject(dir)) {
                await Promise.all(Object.keys(dir).map(async function (key) {
                  if (typeof dir[key] == 'string') {
                    result.files.push(key);
                  } else {
                    result.dirs.push(key);
                    if (current[key].length == 0) {
                      await $.post(url_scan, {directory: rootFolder + path + '/' + key, root: rootFolder}, function (data) {
                        current[key] = data;
                      });
                    }
                  }
                }));
              }
              return $.when(result); // resolved promise
            },
            open: function (filename) {
              var file = get(filename);
              if (typeof file == 'string') {
                let link = document.createElement('a');
                link.href = url + file.split('web/').pop();
                let extension = link.href.split('.').pop().toLowerCase();
                if (openExtension.includes(extension)) {
                  link.target = '_blank';
                } else if (google_viewer && googledocExtension.includes(extension)) {
                  link.href = urlGoogle + link.href;
                } else {
                  link.download = filename.split('/').pop();
                }
                document.body.appendChild(link);
                link.click();
                link.remove();
              }
            },
          });
          var browser = that.browse();
        });

      });
    }
  };
}(Drupal, jQuery, once));
