(function (Drupal, $, once) {
  'use strict';

  Drupal.behaviors.filebrowser = {
    attach: function (context, settings) {
      $('.browser').each(function () {
        let url_scan = $(this).data('url_scan');
        var field_id = $(this).data('field_id');
        var rootFolder = $(this).data('folder').replace('\\', '/');
        rootFolder = decodeURI(rootFolder);
        $.post(url_scan, function (env) {
          var current = {};
          function get(path) {
            current = env;
            browser.walk(path, function (file) {
              if (current[file] != undefined) {
                current = current[file];
              }
            });
            return current;
          }

          $('.browser').browse({
            root: '/',
            separator: '/',
            contextmenu: true,
            name: 'filestystem',
            menu: function (type) {
              if (type == 'li') {
                return {
                  'Add to field': function ($li) {
                    $('[data-url_field_id="' + field_id + '"]').val(rootFolder + $('.adress-bar input').val() + $li.text());
                    $('[data-text_field_id="' + field_id + '"]').val($li.text());
                    $('.ui-dialog-titlebar-close').click();
                  }
                }
              }
            },
            refresh_timer: 0,
            dir: async function (path) {
              let dir = get(path);
              if (typeof dir == 'string') {
                $('[data-url_field_id="' + field_id + '"]').val(dir);
              }
              var result = {files: [], dirs: []};
              if ($.isPlainObject(dir)) {
                await Promise.all(Object.keys(dir).map(async function (key) {
                  if (typeof dir[key] == 'string') {
                    let fileRender = key; //+`</span><span class="hide">${key}`
                    result.files.push(fileRender);
                  } else {
                    result.dirs.push(key);
                    if (current[key].length == 0) {
                      await $.post(url_scan, { directory: path + '/' + key }, function (data) {
                        current[key] = data;
                      });
                    }
                  }
                }));
              }
              return $.when(result); // resolved promise
            },
            open: function (filename) {
              var file = get(filename);
              if (typeof file == 'string') {
                $('[data-url_field_id="' + field_id + '"]').val(file);
                if (typeof filename == 'string') {
                  let name = filename.split('/').pop();
                  $('[data-text_field_id="' + field_id + '"]').val(name);
                }
                $('.ui-dialog-titlebar-close').click();
              }
            }
          });
          var browser = $('.browser').eq(0).browse();
        });
      });
    }
  };

  Drupal.behaviors.explorerformatter = {
    attach: function (context, settings) {
      $(once('explorer', 'a.explorer', context)).click(function (event) {
        event.preventDefault();
        $('#browserModal').remove();
        let modal = `
        <div class="modal" tabindex="-1" id="browserModal">
<button type="button" data-bs-toggle="modal" data-bs-target="#browserModal" id="browserModalBtn" class="hidden"></button>
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
    <div class="modal-header">
        <div class="spinner-border m-5" role="status">
          <span class="visually-hidden">Loading...</span>
        </div>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="modalClose"></button>
      </div>
      <p class="modal-body browser"></p>
    </div>
  </div>
</div>`;
        let url_scan = $(this).data('url_scan') ?? $(this).attr('scan');
        let url = $(this).data('url');
        let rootFolder = $(this).attr('href').replace('\\', '/').replace(/^\/+/, '');
        let google_viewer = $(this).data('google_viewer');
        let urlGoogle = 'https://docs.google.com/gview?embedded=true&url=';
        let googledocExtension = ['doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx'];
        let openExtension = ['txt', 'pdf', 'odt', 'ods', 'odp', 'csv',
          'jpg', 'jpeg', 'png', 'gif', 'bmp', 'svg',
          'mp3', 'wav', 'aac', 'ogg',
          'mp4', 'webm', 'ogv', 'mov',
        ];
        // Check if the browser is Microsoft Edge
        let isEdge = /Edg\//i.test(navigator.userAgent);
        if (isEdge) {
          // If the browser is Microsoft Edge, add docx, xlsx, pptx extensions to openExtension
          openExtension.push('docx', 'xlsx', 'pptx');
        }
        rootFolder = decodeURI(rootFolder);
        $(modal).insertAfter($(this));
        $('#browserModalBtn').click();
        var current = {};
        $.post(url_scan, {directory: rootFolder}, function (env) {
          function get(path) {
            current = env;
            browser.walk(path, function (file) {
              if (current[file] != undefined) {
                current = current[file];
              }
            });
            return current;
          }

          $('.modal-header .spinner-border').remove();
          $('p.browser').browse({
            root: '/',
            separator: '/',
            contextmenu: true,
            name: 'filestystem',
            refresh_timer: 0,
            dir: async function (path) {
              let dir = get(path);
              var result = {files: [], dirs: []};
              if ($.isPlainObject(dir)) {
                await Promise.all(Object.keys(dir).map(async function (key) {
                  if (typeof dir[key] == 'string') {
                    let fileRender = key;// +`</span><span class="hide">${key}`
                    result.files.push(fileRender);
                  } else {
                    result.dirs.push(key);
                    if (current[key].length == 0) {
                      await $.post(url_scan, {directory: rootFolder + path + '/' + key}, function (data) {
                        current[key] = data;
                      });
                    }
                  }
                }));
              }
              return $.when(result); // resolved promise
            },
            open: function (filename) {
              let file = get(filename);
              if (typeof file == 'string') {
                let link = document.createElement('a');
                link.href = url + file.split('web/').pop();
                let extension = link.href.split('.').pop().toLowerCase();
                if (openExtension.includes(extension)) {
                  link.target = '_blank';
                } else if (google_viewer && googledocExtension.includes(extension)) {
                  link.href = urlGoogle + link.href;
                } else {
                  link.download = filename.split('/').pop();
                }
                document.body.appendChild(link);
                link.click();
                link.remove();
                $('.ui-dialog-titlebar-close').click();
                $('#modalClose').click();
                $('.modal').remove();
              }
            },
            close: function () {
              $('p.browser').remove();
            }
          });
          var browser = $('.browser').eq(0).browse();
        });

      });

    }
  };
}(Drupal, jQuery, once));
