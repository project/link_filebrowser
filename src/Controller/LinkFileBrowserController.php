<?php

namespace Drupal\link_filebrowser\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StreamWrapper\PublicStream;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for Link filebrowser routes.
 */
class LinkFileBrowserController extends ControllerBase {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilder
   */
  protected $renderer;

  /**
   * Link file browser Controller constructor.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The render service.
   */
  public function __construct(RendererInterface $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('renderer')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function listFiles(Request $request) {
    $directory = str_replace('//', '/', $request->request->get('directory'));
    $path = [
      DRUPAL_ROOT,
      PublicStream::basePath(),
      $request->request->get('root'),
      $directory,
    ];
    $all = (bool) $request->request->get('all');
    $maxLevel = !empty($all) ? FALSE : 2;
    $response = $this->scan(implode('/', $path), $maxLevel);
    return new JsonResponse($response);
  }

  /**
   * {@inheritDoc}
   */
  public function showModalFileExplorer($field_id) {

    $response = new AjaxResponse();
    $title = $this->t('Files in shares folder');
    [$entity_type, $entity_bundle, $field_name] = explode('.', $field_id);
    $url = Url::fromRoute('link_filebrowser.scan', [
      'entity_type' => $entity_type,
      'entity_bundle' => $entity_bundle,
      'field_name' => $field_name,
    ]);
    // Attach the library necessary for using the Open(Modal)DialogCommand and
    // set the attachments for this Ajax response.
    $content['#attached']['library'][] = 'core/drupal.dialog.ajax';
    $content['text'] = [
      '#theme' => 'link_file_browser_popup',
      '#field_id' => $field_id,
      '#url_scan' => $url->toString(),
      '#folder' => '/' . $this->getFolder($entity_type, $entity_bundle, $field_name),
    ];
    $options = [
      'dialogClass' => 'views-ui-dialog js-views-ui-dialog',
      'width' => '75%',
    ];
    $response->addCommand(new OpenModalDialogCommand($title, $content, $options));
    return $response;
  }

  /**
   * {@inheritDoc}
   */
  public function scanFiles(Request $request, $entity_type, $entity_bundle, $field_name) {
    // Output the directory listing as JSON.
    $form_mode = 'default';
    $temp = explode('-', $field_name);
    $field_name = $temp[0];
    $settings = $this->entityTypeManager()
      ->getStorage('entity_form_display')
      ->load($entity_type . '.' . $entity_bundle . '.' . $form_mode)
      ->getComponent($field_name)['settings'];
    $maxLevel = (bool) $settings['all'] ? FALSE : 2;
    $directory = str_replace('//', '/', $request->request->get('directory'));
    if (!empty($directory) && file_exists($directory)) {

    }
    else {
      $directory = $this->getFolder($entity_type, $entity_bundle, $field_name, $directory);
    }
    $response = $this->scan($directory, $maxLevel);
    return new JsonResponse($response);
  }

  /**
   * {@inheritDoc}
   */
  public function getFolder($entity_type, $entity_bundle, $field_name, $directory = '') {
    $temp = explode('-', $field_name);
    $field_name = $temp[0];
    $form_mode = 'default';
    $field_widget_settings = $this->entityTypeManager()
      ->getStorage('entity_form_display')
      ->load($entity_type . '.' . $entity_bundle . '.' . $form_mode)
      ->getComponent($field_name)['settings'];
    if (stripos($field_widget_settings["folder"], '{') !== FALSE) {
      $renderable = [
        '#type' => 'inline_template',
        '#template' => $field_widget_settings["folder"],
        '#context' => [
          'entity_type' => $entity_type,
          'entity_bundle' => $entity_bundle,
          'field_name' => $field_name,
        ],
      ];
      $field_widget_settings["folder"] = $this->renderer->render($renderable)->__toString();
    }
    $field_widget_settings["folder"] .= $directory;
    $directory = !empty($field_widget_settings["folder"]) ? '/' . $field_widget_settings["folder"] : DIRECTORY_SEPARATOR;
    // Get directory in field widget config.
    $path = PublicStream::basePath();
    return $path . $directory;
  }

  /**
   * Get files in directory.
   *
   * {@inheritDoc}
   */
  public function scan($dir, $maxLevel = 2, $level = 0) {

    $files = [];
    $level++;
    // Is there actually such a folder/file?
    if (file_exists($dir)) {
      foreach (scandir($dir) as $f) {
        if (!$f || $f[0] == '.') {
          // Ignore hidden files.
          continue;
        }
        if (is_dir($dir . '/' . $f)) {
          // The path is a folder.
          $files[$f] = [];
          if (!$maxLevel || $level < $maxLevel) {
            $files[$f] = $this->scan($dir . '/' . $f, $maxLevel, $level);
          }
        }
        else {
          // It is a file.
          $file_path = '/' . $dir . '/' . $f;
          $extension = pathinfo($file_path, PATHINFO_EXTENSION);
          if ($extension !== 'php') {
            $files[$f] = $file_path;
          }
        }
      }
    }
    return $files;
  }

}
