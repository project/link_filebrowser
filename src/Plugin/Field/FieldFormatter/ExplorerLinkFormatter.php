<?php

namespace Drupal\link_filebrowser\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;

/**
 * Plugin implementation of the 'explorer_link' formatter.
 */
#[FieldFormatter(
  id: 'explorer_link',
  label: new TranslatableMarkup('Link with File browser'),
  field_types: [
    'link',
  ],
)]
class ExplorerLinkFormatter extends LinkFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'google_viewer' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['google_viewer'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("View file with google viewer"),
      '#default_value' => $this->getSetting('google_viewer'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    if ($this->getSetting('google_viewer')) {
      $summary['google_viewer'] = $this->t("View file with google viewer");
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    $settings = $this->getSettings();
    foreach ($elements as $delta => &$element) {
      $url = $element["#url"];
      if (!$url->isRouted()) {
        $uri = str_replace('base:', '', $url->getUri());
        // @phpstan-ignore-next-line
        $realpath = \Drupal::service('file_system')->realpath($uri);
        $entity = $items->getEntity();
        if (is_dir($realpath)) {
          $elements[$delta]["#options"]["attributes"]['class'][] = 'explorer';
          $url = Url::fromRoute('link_filebrowser.scan', [
            'entity_type' => $entity->getEntityTypeId(),
            'entity_bundle' => $entity->bundle(),
            'field_name' => $items->getName(),
          ]);
          $elements[$delta]["#options"]["attributes"]['data-url_scan'] = $url->toString();
          $elements[$delta]["#options"]["attributes"]['data-google_viewer'] = $settings['google_viewer'] ? 'true' : 'false';
          $elements[$delta]["#options"]["attributes"]['data-url'] = trim(Url::fromUserInput('/', ['absolute' => TRUE])->toString(), '/');
        }
      }
    }
    $elements['#attached']['library'][] = 'link_filebrowser/browser';
    return $elements;
  }

}
