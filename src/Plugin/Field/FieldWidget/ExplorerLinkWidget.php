<?php

namespace Drupal\link_filebrowser\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;

/**
 * Defines the 'explorer_link' field widget.
 */
#[FieldWidget(
  id: 'explorer_link',
  label: new TranslatableMarkup('Link with File browser'),
  description: new TranslatableMarkup('Link with File browser to select folder or file.'),
  field_types: ['link'],
)]
class ExplorerLinkWidget extends LinkWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'folder' => '',
      'all' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $element['folder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path folder to browser'),
      '#default_value' => $this->getSetting('folder'),
      '#required' => TRUE,
      '#description' => $this->t('Folder must be in public://.'),
    ];
    $element['all'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Load all subfolders/files'),
      '#default_value' => $this->getSetting('all'),
      '#description' => $this->t('Use for small directory'),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $baseId = $this->fieldDefinition->get('id');
    $id = $baseId . '-' . $delta;
    $options = [
      'attributes' => [
        'class' => [
          'button',
          'button-action',
          'button--primary',
          'button--small',
          'btn-explorer',
          'btn',
          'btn-sm',
          'btn-primary',
          'use-ajax',
        ],
        'role' => "button",
        'id' => $id,
      ],
    ];
    $url = Url::fromRoute('link_filebrowser.explorer', ['field_id' => $id], $options);
    $link = Link::fromTextAndUrl($this->t('FILE BROWSER'), $url);
    $description = $this->t('For a document or a folder hosted on a file server, click on the button FILE BROWSER');
    $element["uri"]["#description"] = $description . '<br/>' . $link->toString();
    $element["uri"]["#attributes"]["data-url_field_id"] = $id;
    $element["title"]["#attributes"]["data-text_field_id"] = $id;
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = $this->t('Folder: @folder', ['@folder' => $this->getSetting('folder')]);
    return $summary;
  }

}
