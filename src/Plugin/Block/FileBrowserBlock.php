<?php

namespace Drupal\link_filebrowser\Plugin\Block;

use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a file browser block.
 */
#[Block(
  id: "link_filebrowser",
  admin_label: new TranslatableMarkup("File browser"),
  category: new TranslatableMarkup("File browser")
)]
class FileBrowserBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilder
   */
  protected $renderer;

  /**
   * File browser block constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The render service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'folder' => '',
      'all' => FALSE,
      'google_viewer' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['folder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path folder to browser'),
      '#default_value' => $this->configuration['folder'],
      '#required' => TRUE,
      '#description' => $this->t('Folder must be in public://.'),
    ];
    $form['all'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Load all subfolders/files'),
      '#default_value' => $this->configuration['all'],
      '#description' => $this->t('Use for small directory'),
    ];
    $form['google_viewer'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("View file with google viewer"),
      '#default_value' => $this->configuration['google_viewer'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configuration['folder'] = $values['folder'];
    $this->configuration['all'] = $values['all'];
    $this->configuration['google_viewer'] = $values['google_viewer'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $url = Url::fromRoute('link_filebrowser.list_files');
    if (stripos($config['folder'], '{') !== FALSE) {
      $renderable = [
        '#type' => 'inline_template',
        '#template' => $config['folder'],
        '#context' => $config,
      ];
      $config['folder'] = $this->renderer->render($renderable)->__toString();
    }
    $build['content'] = [
      '#theme' => 'link_file_browser_block',
      '#url_scan' => $url->toString(),
      '#folder' => $config['folder'],
      '#all' => $config['all'],
      '#google_viewer' => $config['google_viewer'],
      '#url' => trim(Url::fromUserInput('/', ['absolute' => TRUE])->toString(), '/'),
      '#attributes' => [
        'class' => [
          'browser-block',
        ],
      ],
    ];
    $build['#attached']['library'][] = 'link_filebrowser/browser.block';

    return $build;
  }

}
